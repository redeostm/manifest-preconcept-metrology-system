%{
findROC.m

This script aims to find the true ROC of field plate, including the calculation error.
%}
function [] = findROC(cameraNum)

	% Read prefix (e.g. MANIFEST_sim_001_coord_data_camera-1) and number of grids to make the map
	prefix = sprintf('MANIFEST_sim_001_coord_data_camera-%d',cameraNum);
	
	% Read the (Xplt,Yplt,Zplt)
	arrXYZPlt = ReadPlatePos(prefix);

	% Calculate the ROC for each (Xplt,Yplt,Zplt)
	arrROC = CalcROC(arrXYZPlt);

	% Write the ROC
	WriteROCXYZ(arrXYZPlt,arrROC,prefix);
end

%{
Read the plate positions.

Input:
	- prefix : prefix of the input file (filename == input/[prefix].dat)

Output
	- arrPlt : X/Y/Z plate positions in units of mm.
%}
function ret = ReadPlatePos(prefix)

	rawData = load(sprintf('input/%s.dat',prefix),'-ascii');
	arrPlt = rawData(:,3:5);
	arrPix = rawData(:,6:7);

	ret = arrPlt;
end


%{
Calculate ROC estimation for each X/Y/Z plate coordinate.

Input:
	- arrPlt : X/Y/Z plate coordinates, in units of mm.

Output:
	- arrROC : ROC estimated from plate coordinates, in units of mm.
%}
function ret = CalcROC(arrPlt)

	len = length(arrPlt);
	arrROC = zeros(len);

	for indx = 1:len
		arrROC(indx) = (-(arrPlt(indx,1)^2+arrPlt(indx,2)^2+arrPlt(indx,3)^2)/(2.*arrPlt(indx,3)));
	end

	ret = arrROC;
end


%{
Write the estimated ROC for each X/Y/Z plate position.

Input:
	- arrPlt : X/Y/Z plate coordinates, in units of mm
	- arrROC : Estimated ROCs, in units of mm
	- prefix : prefix of output file (filename == "output/[prefix].ROC")

Output: None (output file is written)
%}
function [] = WriteROCXYZ(arrPlt,arrROC,prefix)
	
	fp = fopen(sprintf('output/%s.ROC',prefix),'w');
	fprintf(fp,'# COL 1-3 : X/Y/Z plate coordinate [mm]\n');
	fprintf(fp,'# COL 4 : Estimated ROC [mm]\n');

	for indx = 1:length(arrPlt)
		fprintf(fp,'%f  %f  %f  %f\n',arrPlt(indx,1),arrPlt(indx,2),arrPlt(indx,3),arrROC(indx));
	end

	fclose(fp);
end

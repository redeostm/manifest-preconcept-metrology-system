%{
findPlatePos.m

This script aims to find the world coordinate (X/Y/Z in units of mm) from the pixel coordinate (X/Y in units of px),
by assuming that the all plate alignment/camera distortion parameters are known.
%}
function [] = findPlatePos(cameraNum,Ngrid)

	% Read prefix (e.g. MANIFEST_sim_001_coord_data_camera-1) and number of grids to make the map
	prefix = sprintf('MANIFEST_sim_001_coord_data_camera-%d',cameraNum);
	
	% Perform the fitting
	tic ();
	GetPlateCoordinate(prefix,'linear',Ngrid);
	fprintf('Kind = linear : %f seconds\n',toc ());
    
    tic ();
    GetPlateCoordinate(prefix,'natural',Ngrid);
    fprintf('Kind = natural : %f seconds\n',toc ());
    
end


%{
Main function to get the plate position

Input:
	- prefix : prefix of input/output files
	- kind : kind of interpolation ('linear','cubic','quintic' for Python scipy; 'linear','natural' for MATLAB)

	- Ngrid : number of grid points

Output: None (output file is written)
%}
function [] = GetPlateCoordinate(prefix,kind,Ngrid)

	% Read (1) plate alignment/camera distortion parameters, (2) pixel coordinates and (3) true world coordinates
	[arrPix,arrPltReal] = ReadInput(sprintf('output/%s.newInput',prefix));
	fprintf('Shape of arrPix: ');
	size(arrPix)
	fprintf('Shape of arrPltReal: ');
	size(arrPltReal)

	params = GetParameter(prefix);

	% Construct the (Xpix,Ypix) ---> (Xplt,Yplt) map by using pixeled map
	[mapXYpixToXplt,mapXYpixToYplt] = MakeMapPixToPlate(params,kind,Ngrid);

	% Convert the pixel coordinate to world coordinate (X/Y/Z)
	arrPltEst = ConvertPixToPlate(arrPix,mapXYpixToXplt,mapXYpixToYplt,params);

	% Write the results by comparing the true world coordinates
	WriteOutput(sprintf('output/%s.%s.%04d',prefix,kind,Ngrid),arrPltEst,arrPltReal);

end


%{
Read the input file and returns pixel (and plate) positions.

Input:
	- filename : name of the input file.
	- isReference : True if one wants to get the actual plate positions too.

Output:
	- arrPix : X/Y pixel positions in units of px.
	- arrXYZ : actual X/Y/Z plate positions in units of mm; available only if isReference == True.
%}
function [arrPix,arrXYZ] = ReadInput(filename)

    nHeader = 2;    % number of headers --- change it as you want.
    nXYZStart = 1;  % starting column number of plate positions --- change it as you want.
    nPixStart = 4;  % starting column number of pixel positions --- change it as you want.

    rawData = importdata(filename,' ',nHeader);
	arrXYZ = rawData.data(:,nXYZStart:nXYZStart+2);
	arrPix = rawData.data(:,nPixStart:nPixStart+1);

end


%{
Get the plate alignemtn/camera distortion parameters.
Note that it is manually obtained from Michael's data headers.

Input:
	- prefix : prefix of input/output files; should be "MANIFEST_sim_001_coord_data_camera-#".

Output:
	- params : class of Parameter() that contains ROC,fc,TC,RC,kc,cc
%}
function params = GetParameter(prefix)

	params.ROC = 3265.; % Radius of Curvature in units of mm
	params.DFOV = 1200.; % Approximate diameter of plate in units of mm
	params.fc = 7246.376812; % image focal distance in units of pixels
	params.Tc = [0, -299.408892, -3261.955597]; % Translation in units of mm
	params.Rc = [0, 0, 0]; % Rotation in units of radians
	params.kc = [-0.2, 0, 0, 0, 0]; % radial distortion coefficients
	params.cc = [1224, 1025]; % image center in units of pixels

	if strcmp(prefix,'MANIFEST_sim_001_coord_data_camera-1')
		params.Rc = [0.097242, -0.097242, 1.568608];
	elseif strcmp(prefix,'MANIFEST_sim_001_coord_data_camera-2')
		params.Rc = [0.120983, 0.032417, -0.522923];
	elseif strcmp(prefix,'MANIFEST_sim_001_coord_data_camera-3')
		params.Rc = [0.043415, 0.162026, -2.613643];
	end
	
end


%{
Returns two maps : (Xpix,Ypix) ---> (Xplt) and (Xpix,Ypix) ---> (Yplt)
from given plate alignment/camera distortion parameters and spline interpolation setup.

Input:
	- params : Parameter() object that contains plate alignment/camera distortion parameters.
	- kind : kind of interpolation ('linear','cubic','quintic' for Python scipy; 'linear','natural' for MATLAB)
	- Ngrid : number of grids from -DFOV/2 to +DFOV/2

Output:
	- mapXYpixToXplt : mapping function of (Xpix,Ypix) ---> (Xplt)
	- mapXYpixToYplt : mapping function of (Xpix,Ypix) ---> (Yplt)
%}
function [mapXYpixToXplt,mapXYpixToYplt] = MakeMapPixToPlate(params,kind,Ngrid)
%function [Xplt,Yplt,XYpix] = MakeMapPixToPlate(params,kind,Ngrid)

	% Step 1. Make the grid for X_plate and Y_plate
	[Xplt,Yplt] = ndgrid(linspace(-params.DFOV*0.55,params.DFOV*0.55,Ngrid),linspace(-params.DFOV*0.55,params.DFOV*0.55,Ngrid));
	Xplt = reshape(Xplt,[],1);
	Yplt = reshape(Yplt,[],1);

	% Step 2. Calculate the Z_plate for each X_plate and Y_plate, and stack it
	Zplt = sqrt(params.ROC^2-Xplt.^2-Yplt.^2) - params.ROC;
	XYZplt = horzcat(Xplt,Yplt,Zplt);

	% Step 3. Project 3D points to an image plane, by using OpenCV
	cameraMatrix = [params.fc, 0, params.cc(1); 0, params.fc, params.cc(2); 0, 0, 1];
	addpath(genpath('./mexopencv'));		% this is to run OpenCV in my PC
	[XYpix,Jacob] = cv.projectPoints(XYZplt,params.Rc,params.Tc,cameraMatrix,'DistCoeffs',params.kc);
	XYpix = reshape(XYpix,[],2);

	% Step 4. Construct maps to find (Xpix,Ypix) ---> (Xplt) and (Xpix,Ypix) ---> (Yplt)
	mapXYpixToXplt = scatteredInterpolant(XYpix,Xplt,kind);
	mapXYpixToYplt = scatteredInterpolant(XYpix,Yplt,kind);
	

end


%{
Returns X/Y/Z plate position from a given X/Y camera position and mappin functions.

Input:
	- arrPixPos : X/Y camera position in units of px.
	- mapXYpixToXplt : mapping function of (Xpix,Ypix) ---> (Xplt)
	- mapXYpixToYplt : mapping function of (Xpix,Ypix) ---> (Yplt)
	- params : Parameter() object that contains plate alignment/camera distortion parameters.

Output:
	- XYZplt : X/Y/Z plate position in units of mm.
%}
function XYZplt = ConvertPixToPlate(arrPix,mapXYpixToXplt,mapXYpixToYplt,params)
%function XYZplt = ConvertPixToPlate(arrPix,XOplt,YOplt,XYOpix,params,kind)

	% Step 1. Get the Xplt and Yplt
	Xplt = mapXYpixToXplt(arrPix);
	Yplt = mapXYpixToYplt(arrPix);
	%{
	Xplt = griddatan(XYOpix,XOplt,arrPix,kind);
	Yplt = griddatan(XYOpix,YOplt,arrPix,kind);
    %}

	% Step 2. Get the Zplt from Xplt and Yplt, and stack it
	Zplt = sqrt(params.ROC^2-Xplt.^2-Yplt.^2) - params.ROC;
	XYZplt = horzcat(Xplt,Yplt,Zplt);

end


%{
Write the estimated X/Y/Z world coordinates from the pixel coordinates.

Input:
	- filename : name of the output file.
	- arrEst : estimated X/Y/Z world coordinates.
	- reference : if it is not None, the true X/Y/Z world coordinates.

Output: none (output file is written)
%}
function [] = WriteOutput(filename,arrEst,reference)

	fp = fopen(filename,'w');
	fprintf(fp,'# COL 1-3: Estimated X/Y/Z plate position [mm]\n');

	if nargin == 3
		fprintf(fp,'# COL 4-6: X/Y/Z plate position error [mm]\n');
		for indx = 1:length(arrEst)
			fprintf(fp,'%f  %f  %f  %f  %f  %f\n',arrEst(indx,1),arrEst(indx,2),arrEst(indx,3),arrEst(indx,1)-reference(indx,1),arrEst(indx,2)-reference(indx,2),arrEst(indx,3)-reference(indx,3));
		end
	else
		for indx = 1:length(arrEst)
			fprintf(fp,'%f  %f  %f\n',arrEst(indx,1),arrEst(indx,2),arrEst(indx,3));
		end
	end
		
	fclose(fp);
end

#!/usr/bin/python3

"""
checkDistortionEq.py

This script aims to check whether a given X/Y/Z plate position really transform into the designed X/Y camera position
through a given plate alignment/camera distortion parameters.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
from scipy import interpolate as itpl
from astropy.modeling import models,fitting
import cv2
import time,sys

class Parameter:
	def __init__(self):
		self.ROC = 3265.			# Radius of Curvature in units of mm
		self.DFOV = 1200.			# Approximate diameter of plate in units of mm
		self.fc = 7246.376812	# image focal distance in units of pixels
		self.Tc = np.array([0,-299.408892,-3261.955597])		# Translation in units of mm
		self.Rc = np.array([0,0,0])	# Rotation in units of rad...
		self.kc = np.array([-0.2,0,0,0,0])		# radial distortion coefficients
		self.cc = np.array([1224,1025])				# image center in units of pixels



"""
Main function to transform (Xplt,Yplt,Zplt) ---> (Xpix,Ypix).

Input:
	- prefix : prefix of input/output files

Output: None (output file is written)
"""
def TransformPlateToPixel(prefix):

	"""Read the input file to get (1) plate position, and (2) true pixel position"""
	arrPlate,arrPixReal = ReadInput("input/%s.dat" % (prefix),isReference=True)

	"""Get the plate alignment/camera distortion parameters"""
	params = GetParameter(prefix)

	"""Estimate the pixel positions from the plate position"""
	arrPixEst = ConvertPlateToPix(arrPlate,params)

	"""Write the results by comparing the true pixel position"""
	WriteOutput("output/%s.pixComp" % (prefix),arrPixEst,reference=arrPixReal)




"""
Read the input file and returns plate (and pixel) positions.

Input:
	- filename : name of the input file.
	- isReference : True if one wants to get the true pixel positions too.

Output:
	- arrXYZ : X/Y/Z plate positions in units of mm.
	- arrPix : true X/Y pixel positions in units of px; only available if isReference==True.
"""
def ReadInput(filename,isReference=False):

	rawData = np.genfromtxt(filename)
	arrXYZ,arrPix = rawData[:,2:5],rawData[:,5:7]

	if isReference == True:
		return arrXYZ,arrPix
	else:
		return arrXYZ



"""
Get the plate alignemtn/camera distortion parameters.
Note that it is manually obtained from Michael's data headers.

Input:
	- prefix : prefix of input/output files; should be "MANIFEST_sim_001_coord_data_camera-#".

Output:
	- params : class of Parameter() that contains ROC,fc,TC,RC,kc,cc
"""
def GetParameter(prefix):

	params = Parameter()
	params.ROC = 3265.			# Radius of Curvature in units of mm
	params.DFOV = 1200.			# Approximate diameter of plate in units of mm
	params.fc = 7246.376812	# image focal distance in units of pixels
	params.Tc = np.array([0,-299.408892,-3261.955597])		# Translation in units of mm

	params.Rc = np.array([0,0,0])	# Rotation in units of rad... see below
	if prefix == "MANIFEST_sim_001_coord_data_camera-1":
		params.Rc = np.array([0.097242,-0.097242,1.568608])
	elif prefix == "MANIFEST_sim_001_coord_data_camera-2":
		params.Rc = np.array([0.120983,0.032417,-0.522923])
	if prefix == "MANIFEST_sim_001_coord_data_camera-3":
		params.Rc = np.array([0.043415,0.162026,-2.613643])

	params.kc = np.array([-0.2,0,0,0,0])		# radial distortion coefficients
	params.cc = np.array([1224,1025])				# image center in units of pixels


	return params



"""
Returns (Xpix,Ypix) from a given (Xplt,Yplt,Zplt) and plate alignment/camera distortion parameters.

Input:
	- arrPlate : X/Y/Z plate position in units of mm.
	- params : Parameter() class object that contains plate aligment/camera distortion parameters.

Output:
	- arrPix : X/Y pixel position in units of pix.
"""
def ConvertPlateToPix(arrPlate,params):

	"""Step 1. Check if a given (Xplt,Yplt,Zplt) satisfies ROC"""
	maxDiffROC = np.abs(np.sqrt(arrPlate[:,0]**2+arrPlate[:,1]**2+(arrPlate[:,2]+params.ROC)**2)-params.ROC).max()
	print("Maximum difference between (Xplt,Yplt,Zplt)-derived ROC and actual ROC : %f" % (maxDiffROC))
	print("Dimension of arrPlate: ",arrPlate.shape)
	print("arrPlate: ",arrPlate)

	"""Step 2. Project 3D points to an image plane, by using OpenCV"""

	"""0_noChange"""
	cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]])
	arrPix,jac = cv2.projectPoints(np.copy(arrPlate),params.Rc,params.Tc,cameraMatrix,params.kc)
	arrPix = arrPix.reshape(-1,2)
	"""1_changeCameraMatrix	-- TOO BAD
	cameraMatrix = np.array([[params.fc,0,0],[0,params.fc,0],[params.cc[0],params.cc[1],1]])
	distCoeffs = np.array([params.kc[0],params.kc[1],params.kc[4],params.kc[2],params.kc[3]])
	"""
	"""2_XYZ_Rodrigues
	cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]])
	distCoeffs = np.array([params.kc[0],params.kc[1],params.kc[4],params.kc[2],params.kc[3]])
	RotZ = np.mat([[np.cos(params.Rc[2]),-np.sin(params.Rc[2]),0],[np.sin(params.Rc[2]),np.cos(params.Rc[2]),0],[0,0,1]])
	RotY = np.mat([[np.cos(params.Rc[1]),0,np.sin(params.Rc[1])],[0,1,0],[-np.sin(params.Rc[1]),0,np.cos(params.Rc[1])]])
	RotX = np.mat([[1,0,0],[0,np.cos(params.Rc[0]),-np.sin(params.Rc[0])],[0,np.sin(params.Rc[0]),np.cos(params.Rc[0])]])
	RotMat = RotZ*RotY*RotX
	print("RotMat: ",RotMat)
	rvec,jac = cv2.Rodrigues(RotMat)
	print("rvec: ",rvec)
	"""
	"""3_ZYX_Rodrigues
	cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]])
	RotZ = np.mat([[np.cos(params.Rc[2]),-np.sin(params.Rc[2]),0],[np.sin(params.Rc[2]),np.cos(params.Rc[2]),0],[0,0,1]])
	RotY = np.mat([[np.cos(params.Rc[1]),0,np.sin(params.Rc[1])],[0,1,0],[-np.sin(params.Rc[1]),0,np.cos(params.Rc[1])]])
	RotX = np.mat([[1,0,0],[0,np.cos(params.Rc[0]),-np.sin(params.Rc[0])],[0,np.sin(params.Rc[0]),np.cos(params.Rc[0])]])
	RotMat = RotX*RotY*RotZ
	print("RotMat: ",RotMat)
	rvec,jac = cv2.Rodrigues(RotMat)
	print("rvec: ",rvec)
	"""
	"""4_XYZ_RotMatTranspose -- TOO BAD
	cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]])
	RotZ = np.mat([[np.cos(params.Rc[2]),-np.sin(params.Rc[2]),0],[np.sin(params.Rc[2]),np.cos(params.Rc[2]),0],[0,0,1]])
	RotY = np.mat([[np.cos(params.Rc[1]),0,np.sin(params.Rc[1])],[0,1,0],[-np.sin(params.Rc[1]),0,np.cos(params.Rc[1])]])
	RotX = np.mat([[1,0,0],[0,np.cos(params.Rc[0]),-np.sin(params.Rc[0])],[0,np.sin(params.Rc[0]),np.cos(params.Rc[0])]])
	RotMat = RotZ*RotY*RotX
	print("RotMat: ",RotMat)
	rvec,jac = cv2.Rodrigues(RotMat.T)
	print("rvec: ",rvec)
	arrNewPlate = np.copy(arrPlate)
	arrPix,jac = cv2.projectPoints(arrNewPlate,rvec,params.Tc,cameraMatrix,params.kc)
	arrPix = arrPix.reshape(-1,2)
	print("Dimension of arrPix : ",arrPix.shape)
	"""
	"""5_XYZ_manualCalc
	print("dimension of arrPlate: ",arrPlate.shape)
	RotZ = np.mat([[np.cos(params.Rc[2]),-np.sin(params.Rc[2]),0],[np.sin(params.Rc[2]),np.cos(params.Rc[2]),0],[0,0,1]])
	RotY = np.mat([[np.cos(params.Rc[1]),0,np.sin(params.Rc[1])],[0,1,0],[-np.sin(params.Rc[1]),0,np.cos(params.Rc[1])]])
	RotX = np.mat([[1,0,0],[0,np.cos(params.Rc[0]),-np.sin(params.Rc[0])],[0,np.sin(params.Rc[0]),np.cos(params.Rc[0])]])
	arrXc = params.Tc + np.array(RotZ*RotY*RotX*np.mat(arrPlate.T)).T
	print("dimension of arrXc: ",arrXc.shape)

	arrXn = np.array([[x/z,y/z] for x,y,z in arrXc])
	print("dimension of arrXn: ",arrXn.shape)
	arrRsq = arrXn[:,0]**2 + arrXn[:,1]**2
	arrXd = np.array([[(1+params.kc[0]*rr+params.kc[1]*rr**2+params.kc[4]*rr**3)*x+2*params.kc[2]*x*y+params.kc[3]*(rr+2*x**2),\
					(1+params.kc[0]*rr+params.kc[1]*rr**2+params.kc[4]*rr**3)*y+params.kc[2]*(rr+2*y**2)+2*params.kc[3]*x*y] \
					for x,y,rr in zip(arrXn[:,0],arrXn[:,1],arrRsq)])
	print("dimension of arrXd: ",arrXd.shape)
	arrPix = arrXd*params.fc + params.cc.T
	print("dimension of arrXp: ",arrPix.shape)
	"""
	"""6_manualCalc_Rodrigues
	RotMat,_ = cv2.Rodrigues(np.copy(params.Rc))
	print("shape of RotMat: ",RotMat.shape)
	print("RotMat: ",RotMat)
	arrXc = params.Tc + np.array(RotMat*np.mat(arrPlate.T)).T

	arrXnRsq = [[x/z,y/z,(x**2+y**2)/z**2] for x,y,z in arrXc]
	arrXd = np.array([[(1+params.kc[0]*rr+params.kc[1]*rr**2+params.kc[4]*rr**3)*x+2*params.kc[2]*x*y+params.kc[3]*(rr+2*x**2),\
					(1+params.kc[0]*rr+params.kc[1]*rr**2+params.kc[4]*rr**3)*y+params.kc[2]*(rr+2*y**2)+2*params.kc[3]*x*y] \
					for x,y,rr in arrXnRsq])
	print("dimension of arrXd: ",arrXd.shape)
	arrPix = arrXd*params.fc + params.cc.T
	print("dimension of arrXp: ",arrPix.shape)
	"""


	return arrPix




"""
Write the output pixel position estimation.

Input:
	- filename : output file name.
	- arrXY : pixel position estimated from the plate position.
	- reference : if it is not None, the true pixel position.

Output: none (output file is written)
"""
def WriteOutput(filename,arrXY,reference=None):

	fp = open(filename,"w")
	fp.write("# COL 1-2: Estimated X/Y pixel position [pix]\n")
	if reference is not None:
		fp.write("# COL 3-4: X/Y pixel position error [pix]\n")
		for xyEst,xyReal in zip(arrXY,reference):
			fp.write("%f  %f  %f  %f\n" %
					(xyEst[0],xyEst[1],\
					 xyEst[0]-xyReal[0],xyEst[1]-xyReal[1]))

	else:
		for xyEst in arrXY:
			fp.write("%f  %f\n" % (xyEst[0],xyEst[1]))
		
	fp.close()



if __name__=="__main__":

	"""Read prefix (e.g. MANIFEST_sim_001_coord_data_camera-1) and number of grids to make the map"""
	prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))

	"""Perform the (Xplt,Yplt,Zplt) ---> (Xpix,Ypix) transform and write"""
	TransformPlateToPixel(prefix)

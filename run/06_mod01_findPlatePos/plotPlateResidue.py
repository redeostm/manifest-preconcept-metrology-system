import numpy as np
import matplotlib.pyplot as plt
import sys

rawData = np.genfromtxt("output/MANIFEST_sim_001_coord_data_camera-%d.%s.%04d" \
		% (np.int(sys.argv[1]),sys.argv[2],np.int(sys.argv[3])))

arrEst,arrResidue = rawData[:,:3],rawData[:,3:]
arrReal = arrEst - arrResidue

plt.clf()
plt.figure(figsize=(4,4))

plt.quiver(arrReal[:,0],arrReal[:,1],arrResidue[:,0],arrResidue[:,1])

plt.savefig("plots/MANIFEST_sim_001_coord_data_camera-%d.%s.%04d.quiver.pdf" \
		% (np.int(sys.argv[1]),sys.argv[2],np.int(sys.argv[3])),bbox_inches="tight")


plt.clf()
plt.figure(figsize=(4,4))
maxval = max(arrResidue[:,:2].max(),-arrResidue[:,:2].min())
plt.hist(arrResidue[:,0],bins=np.linspace(-maxval,maxval,21),color='C0',histtype='step',label='Xerr [pix]')
plt.hist(arrResidue[:,1],bins=np.linspace(-maxval,maxval,21),color='C1',histtype='step',label='Yerr [pix]')
plt.legend()
plt.savefig("plots/MANIFEST_sim_001_coord_data_camera-%d.%s.%04d.hist.pdf" \
		% (np.int(sys.argv[1]),sys.argv[2],np.int(sys.argv[3])),bbox_inches="tight")


arrXabs,arrYabs = np.abs(arrResidue[:,0]),np.abs(arrResidue[:,1])
arrR = np.sqrt(arrXabs**2 + arrYabs**2)
fp = open("output/MANIFEST_sim_001_coord_data_camera-%d.%s.%04d.stat" \
		% (np.int(sys.argv[1]),sys.argv[2],np.int(sys.argv[3])),"w")
fp.write("# COL 1: Type [X,Y,R]\n")
fp.write("# COL 2-6: Median, 90%, 95%, 99%, 99.865% error [mm]\n")
fp.write("X  %f  %f  %f  %f  %f\n" %
		(np.median(arrXabs),np.percentile(arrXabs,90),np.percentile(arrXabs,95),np.percentile(arrXabs,99),
		 np.percentile(arrXabs,99.865)))
fp.write("Y  %f  %f  %f  %f  %f\n" %
		(np.median(arrYabs),np.percentile(arrYabs,90),np.percentile(arrYabs,95),np.percentile(arrYabs,99),
		 np.percentile(arrYabs,99.865)))
fp.write("R  %f  %f  %f  %f  %f\n" %
		(np.median(arrR),np.percentile(arrR,90),np.percentile(arrR,95),np.percentile(arrR,99),
		 np.percentile(arrR,99.865)))
fp.close()

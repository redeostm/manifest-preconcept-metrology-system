import numpy as np
import matplotlib.pyplot as plt
import sys

rawData = np.genfromtxt("MANIFEST_sim_001_coord_data_camera-%d.pixComp" % (np.int(sys.argv[1])))

arrEst,arrResidue = rawData[:,:2],rawData[:,2:]
arrReal = arrEst - arrResidue

plt.clf()
plt.figure(figsize=(4,4))

plt.quiver(arrReal[:,0],arrReal[:,1],arrResidue[:,0],arrResidue[:,1])

plt.savefig("MANIFEST_sim_001_coord_data_camera-%d.pixComp.pdf" % (np.int(sys.argv[1])),bbox_inches="tight")

plt.clf()
plt.figure(figsize=(4,4))

arrResSqRt = np.array([np.sqrt(x**2+y**2) for x,y in zip(arrResidue[:,0],arrResidue[:,1])])
plt.hist(arrResSqRt,bins=np.linspace(0,arrResSqRt.max(),21),histtype="step",normed=True,\
		label="%f pix (3-sigma)" % (np.percentile(arrResSqRt,99.865)))
plt.legend()

plt.xlabel("Residue [pix]")
plt.ylabel("Frequency")
plt.savefig("MANIFEST_sim_001_coord_data_camera-%d.pixComp.hist.pdf" % (np.int(sys.argv[1])),bbox_inches="tight")

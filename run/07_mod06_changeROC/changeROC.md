
# How the ROC error affect the finding plate positions from pixel positions?
Note that this is one of my first attempt to move from usual Python to Jupyter notebook environment.
If it is good enough, then I will actively move onto this one.

## Sep. 5th
By using the ``scipy.interpolate.interp2d``, the $3\sigma$ residue error on the (X,Y) plate positions were 38 microns, which already exceeds the system requirement.

Also, I found that the Z-values of plate positions in Michael's simulations are slightly different from what one can estimate from given (X,Y)-values and ROC --- mostly about 0.1 mm.
Of course it is a reasonable error in ``float``, however it may be one of the main reasons for the above residue error.

So, below I test the ROC estimated from the Michael's (X,Y,Z) value.
Since the center of the field plate is defined as (0,0,0), one can estimate the ROC by using the following equation.
\begin{align}
X^2 + Y^2 + ({\rm ROC} + Z)^2 &= {\rm ROC}^2 \\
{\rm ROC} &= -(X^2 + Y^2 + Z^2)/2Z
\end{align}

By considering that the fiducial ROC choice was 3265 mm, this is quite strange.
Probably we need to ``re``-simulate the plate positions and pixel positions.

## Sep. 11th
Let's resimulate the plate position and pixel positions, by assuming the given (X,Y) position and ROC = 3265mm.

From the above equation:
\begin{align}
X^2 + Y^2 + (Z + {\rm ROC})^2 &= {\rm ROC}^2 \\
Z &= \sqrt{{\rm ROC}^2 - X^2 - Y^2} - {\rm ROC}
\end{align}

Here are the summary of the results:

| Number of grids | Spline kind | Error in \#1 ($3\sigma$; um) | Error in \#2 ($3\sigma$; um) | Error in \#3 ($3\sigma$; um) |
|-------|-----|-------|------|-------|
|21 |  linear | 102795.900481| 168369.993821 |  168651.738435 |
| - | cubic | 8774.965926 | 1842.153161 | 1337.135795 |
| - |  quintic | 3.225138 | 0.034563 |  0.030747 |
|51 | linear | 11600628.075833 | 3724.359674 | 3725.970256 |
| - | cubic | 669.522371 | 0.072199 | 0.110623 |
| - | quintic |  0.000032 |  0.060766 |  0.067238 |
|61 | linear | 166955095.610385 | 216404051.921873 | 3526585.274695 |
| - | cubic | 835270.485334 | 1.567046 | 0.046702 |
| - | quintic | 102703.656116 |  0.003726 | 0.047892 |
|71 | linear | 435377082.702264 |  82275.759726 | 79587.332134 |
| - | cubic | 0.002504 |  0.162272 | 0.004404 |
| - | quintic | 0.000036 | 0.018020 | 0.013081 |
|81 | linear | 474459048.737653 | 1273.150903 | 1273.328604 |
| - | cubic | 0.000699 | 0.043760 | 0.014058 |
| - | quintic | 0.007639 |  0.063185 | 0.014688 |
    
Still $61 \times 61$ can give something strange, ``quintic`` with greater than $51 \times 51$ usually gives good results with $3\sigma$ error less than 0.1 microns.

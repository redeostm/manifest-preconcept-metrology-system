#!/usr/bin/python3

"""
findPlatePos.py

This script aims to find the world coordinate (X/Y/Z in units of mm) from the pixel coordinate (X/Y in units of px),
by assuming that the all plate alignment/camera distortion parameters are known.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
from scipy import interpolate as itpl
import cv2
import time,sys

class Parameter:
	def __init__(self):
		self.ROC = 3265.			# Radius of Curvature in units of mm
		self.DFOV = 1200.			# Approximate diameter of plate in units of mm
		self.fc = 7246.376812	# image focal distance in units of pixels
		self.Tc = np.array([0,-299.408892,-3261.955597])		# Translation in units of mm
		self.Rc = np.array([0,0,0])	# Rotation in units of rad...
		self.kc = np.array([-0.2,0,0,0,0])		# radial distortion coefficients
		self.cc = np.array([1224,1025])				# image center in units of pixels



"""
Main function to get the plate position

Input:
	- prefix : prefix of input/output files
	- kind : kind of spline interpolation ('linear','cubic','quintic')
	- Ngrid : number of grid points

Output: None (output file is written)
"""
def GetPlateCoordinate(prefix,kind,Ngrid):

	"""Read (1) plate alignment/camera distortion parameters, (2) pixel coordinates and (3) true world coordinates"""
	arrPix,arrPltReal = ReadInput("input/%s.dat" % (prefix),isReference=True)
	params = GetParameter(prefix)

	"""Construct the (Xpix,Ypix) ---> (Xplt,Yplt) map by using pixeled map"""
	mapXYpixToXplt,mapXYpixToYplt = MakeMapPixToPlate(params,kind,Ngrid)

	"""Convert the pixel coordinate to world coordinate (X/Y/Z)"""
	arrPltEst = ConvertPixToPlate(arrPix,mapXYpixToXplt,mapXYpixToYplt,params)

	"""Write the results by comparing the true world coordinates"""
	WriteOutput("output/%s.%s.%04d" % (prefix,kind,Ngrid),arrPltEst,reference=arrPltReal)




"""
Read the input file and returns pixel (and plate) positions.

Input:
	- filename : name of the input file.
	- isReference : True if one wants to get the actual plate positions too.

Output:
	- arrPix : X/Y pixel positions in units of px.
	- arrXYZ : actual X/Y/Z plate positions in units of mm; available only if isReference == True.
"""
def ReadInput(filename,isReference=False):

	rawData = np.genfromtxt(filename)
	arrXYZ,arrPix = rawData[:,2:5],rawData[:,5:7]

	if isReference == True:
		return arrPix,arrXYZ
	else:
		return arrPix



"""
Get the plate alignemtn/camera distortion parameters.
Note that it is manually obtained from Michael's data headers.

Input:
	- prefix : prefix of input/output files; should be "MANIFEST_sim_001_coord_data_camera-#".

Output:
	- params : class of Parameter() that contains ROC,fc,TC,RC,kc,cc
"""
def GetParameter(prefix):

	params = Parameter()  # See Parameter::__init__ for the definition of other parameters

	params.Rc = np.array([0,0,0])	# Rotation in units of rad... see below
	if prefix == "MANIFEST_sim_001_coord_data_camera-1":
		params.Rc = np.array([0.097242,-0.097242,1.568608])
	elif prefix == "MANIFEST_sim_001_coord_data_camera-2":
		params.Rc = np.array([0.120983,0.032417,-0.522923])
	if prefix == "MANIFEST_sim_001_coord_data_camera-3":
		params.Rc = np.array([0.043415,0.162026,-2.613643])

	return params



"""
Returns two maps : (Xpix,Ypix) ---> (Xplt) and (Xpix,Ypix) ---> (Yplt)
from given plate alignment/camera distortion parameters and spline interpolation setup.

Input:
	- params : Parameter() object that contains plate alignment/camera distortion parameters.
	- kind : kind of spline interpolation ('linear','cubic','quintic')
	- Ngrid : number of grids from -DFOV/2 to +DFOV/2

Output:
	- mapXYpixToXplt : mapping function of (Xpix,Ypix) ---> (Xplt)
	- mapXYpixToYplt : mapping function of (Xpix,Ypix) ---> (Yplt)
"""
def MakeMapPixToPlate(params,kind,Ngrid):

	"""Step 1. Make the grid for X_plate and Y_plate"""
	Xplt,Yplt = np.mgrid[-params.DFOV*0.5:params.DFOV*0.5:Ngrid*1j,-params.DFOV*0.5:params.DFOV*0.5:Ngrid*1j]
	Xplt,Yplt = Xplt.flatten(),Yplt.flatten()
	print("Dimension of Xplt : ",Xplt.shape)

	"""Step 2. Calculate the Z_plate for each X_plate and Y_plate, and stack it"""
	Zplt = np.sqrt(params.ROC**2-Xplt**2-Yplt**2) - params.ROC
	XYZplt = np.vstack([Xplt,Yplt,Zplt]).T
	print("Dimension of XYZplt : ",XYZplt.shape)

	"""Step 3. Project 3D points to an image plane, by using OpenCV"""
	cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]])
	XYpix,Jacob = cv2.projectPoints(XYZplt,params.Rc,params.Tc,cameraMatrix,params.kc)
	XYpix = XYpix.reshape(-1,2)
	print("Dimension of XYpix : ",XYpix.shape)

	"""Step 4. Construct maps to find (Xpix,Ypix) ---> (Xplt) and (Xpix,Ypix) ---> (Yplt)"""
	mapXYpixToXplt = itpl.interp2d(XYpix[:,0],XYpix[:,1],Xplt,kind=kind)
	mapXYpixToYplt = itpl.interp2d(XYpix[:,0],XYpix[:,1],Yplt,kind=kind)

	return mapXYpixToXplt,mapXYpixToYplt




"""
Returns X/Y/Z plate position from a given X/Y camera position and mappin functions.

Input:
	- arrPixPos : X/Y camera position in units of px.
	- mapXYpixToXplt : mapping function of (Xpix,Ypix) ---> (Xplt)
	- mapXYpixToYplt : mapping function of (Xpix,Ypix) ---> (Yplt)
	- params : Parameter() object that contains plate alignment/camera distortion parameters.

Output:
	- XYZplt : X/Y/Z plate position in units of mm.
"""
def ConvertPixToPlate(arrPix,mapXYpixToXplt,mapXYpixToYplt,params):

	"""Step 1. Get the Xplt and Yplt"""
	Xplt = np.array([mapXYpixToXplt(x,y) for x,y in arrPix]).flatten()
	Yplt = np.array([mapXYpixToYplt(x,y) for x,y in arrPix]).flatten()
	print("Dimension of Xplt : ",Xplt.shape)

	"""Step 2. Get the Zplt from Xplt and Yplt, and stack it"""
	Zplt = np.sqrt(params.ROC**2-Xplt**2-Yplt**2) - params.ROC
	XYZplt = np.vstack([Xplt,Yplt,Zplt]).T
	print("Dimension of XYZplt : ",XYZplt.shape)

	return XYZplt



"""
Write the estimated X/Y/Z world coordinates from the pixel coordinates.

Input:
	- filename : name of the output file.
	- arrEst : estimated X/Y/Z world coordinates.
	- reference : if it is not None, the true X/Y/Z world coordinates.

Output: none (output file is written)
"""
def WriteOutput(filename,arrEst,reference=None):

	fp = open(filename,"w")
	fp.write("# COL 1-3: Estimated X/Y/Z plate position [mm]\n")
	if reference is not None:
		fp.write("# COL 4-6: X/Y/Z plate position error [mm]\n")
		for xyzEst,xyzReal in zip(arrEst,reference):
			fp.write("%f  %f  %f  %f  %f  %f\n" %
					(xyzEst[0],xyzEst[1],xyzEst[2],\
					 xyzEst[0]-xyzReal[0],xyzEst[1]-xyzReal[1],xyzEst[2]-xyzReal[2]))

	else:
		for xyzEst in arrEst:
			fp.write("%f  %f  %f\n" % (xyzEst[0],xyzEst[1],xyzEst[2]))
		
	fp.close()



if __name__=="__main__":

	"""Read prefix (e.g. MANIFEST_sim_001_coord_data_camera-1) and number of grids to make the map"""
	prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))
	Ngrid = np.int(sys.argv[2])
	
	"""Perform the fitting for three different kinds of spline interpolation"""
#for kind in ['linear','cubic','quintic']:
for kind in ['quintic']:

		"""Record the starting time to calculate the elapsed time"""
		tstart = time.time()	

		"""Perform the plate position finding and write"""
		GetPlateCoordinate(prefix,kind,Ngrid)

		"""Record the last time and calculate the elapsed time"""
		tend = time.time()
		print("Kind = %s : %f seconds" % (kind,tend-tstart))

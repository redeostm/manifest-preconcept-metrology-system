#!/usr/bin/python3

"""
findROC.py

This script aims to find the true ROC of field plate, including the calculation error.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
from scipy import interpolate as itpl
import time,sys



"""
Read the plate positions.

Input:
	- prefix : prefix of the input file (filename == input/[prefix].dat)

Output
	- arrPlt : X/Y/Z plate positions in units of mm.
"""
def ReadPlatePos(prefix):

	rawData = np.genfromtxt("input/%s.dat" % (prefix))
	arrPlt,arrPix = rawData[:,2:5],rawData[:,5:7]

	return arrPlt



"""
Calculate ROC estimation for each X/Y/Z plate coordinate.

Input:
	- arrPlt : X/Y/Z plate coordinates, in units of mm.

Output:
	- arrROC : ROC estimated from plate coordinates, in units of mm.
"""
def CalcROC(arrPlt): 

	arrROC = []
	for xyz in arrPlt:
		arrROC.append(-(xyz[0]**2+xyz[1]**2+xyz[2]**2)/(2.*xyz[2]))

	return np.array(arrROC)


"""
Write the estimated ROC for each X/Y/Z plate position.

Input:
	- arrPlt : X/Y/Z plate coordinates, in units of mm
	- arrROC : Estimated ROCs, in units of mm
	- prefix : prefix of output file (filename == "output/[prefix].ROC")

Output: None (output file is written)
"""
def WriteROCXYZ(arrPlt,arrROC,prefix):
	
	fp = open("output/%s.ROC" % (prefix),"w")
	fp.write("# COL 1-3 : X/Y/Z plate coordinate [mm]\n")
	fp.write("# COL 4 : Estimated ROC [mm]\n")
	for xyz,roc in zip(arrPlt,arrROC):
		fp.write("%f  %f  %f  %f\n" % (xyz[0],xyz[1],xyz[2],roc))

	fp.close()


if __name__=="__main__":

	"""Read prefix (e.g. MANIFEST_sim_001_coord_data_camera-1) and number of grids to make the map"""
	prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))
	
	"""Read the (Xplt,Yplt,Zplt)"""
	arrXYZPlt = ReadPlatePos(prefix)

	"""Calculate the ROC for each (Xplt,Yplt,Zplt)"""
	arrROC = CalcROC(arrXYZPlt)	

	"""Write the ROC"""
	WriteROCXYZ(arrXYZPlt,arrROC,prefix)

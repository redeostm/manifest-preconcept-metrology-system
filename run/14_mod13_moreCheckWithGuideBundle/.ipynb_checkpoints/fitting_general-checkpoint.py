import numpy as np

from params import *
from plate_pix_pf_gb import *

def ConstructParameter(criticalParam,probableParam,redundantParam):
    params = Parameter()
    TcSq = params.Tc[0]**2 + params.Tc[1]**2 + params.Tc[2]**2
    params.Tc = np.array([criticalParam[0],criticalParam[1],-np.sqrt(TcSq - criticalParam[0]**2 - criticalParam[1]**2)])
    params.Rc = np.array([probableParam[0],probableParam[1],criticalParam[2]])
    params.fc = criticalParam[3]
    params.kc = np.array([probableParam[2],redundantParam[0],redundantParam[1],redundantParam[2],redundantParam[3]])
    params.cc = np.array([criticalParam[4],criticalParam[5]])
    return params

def ResidueChangeCritical(criticalParam,arrPlate,arrPix,probableParam,redundantParam):
    params = ConstructParameter(criticalParam,probableParam,redundantParam)
    arrEstPix = ConvertPlateToPix(arrPlate,params)
    return np.sqrt((arrEstPix[:,0]-arrPix[:,0])**2 + (arrEstPix[:,1]-arrPix[:,1])**2)

def ResidueChangeProbable(probableParam,arrPlate,arrPix,criticalParam,redundantParam):
    params = ConstructParameter(criticalParam,probableParam,redundantParam)
    arrEstPix = ConvertPlateToPix(arrPlate,params)
    return np.sqrt((arrEstPix[:,0]-arrPix[:,0])**2 + (arrEstPix[:,1]-arrPix[:,1])**2)

def ResidueChangeRedundant(redundantParam,arrPlate,arrPix,criticalParam,probableParam):
    params = ConstructParameter(criticalParam,probableParam,redundantParam)
    arrEstPix = ConvertPlateToPix(arrPlate,params)
    return np.sqrt((arrEstPix[:,0]-arrPix[:,0])**2 + (arrEstPix[:,1]-arrPix[:,1])**2)
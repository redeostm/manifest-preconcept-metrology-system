import numpy as np
import matplotlib.pyplot as plt
import cv2
from scipy import interpolate as itpl
import sys

from fitting import *
#from plate_pix_pf_gb import *

import h5py

# Read guide bundles and random variables
f = h5py.File("input/GB_100set.hdf5","r")
plateGBSet = f["PlateGB"][:,:]
f.close()
print("Done reading plateGBSet")

f = h5py.File("input/rand_500set_5percent.hdf5","r")
randVar = f["RandVar"][:,:]
f.close()
print("Done reading randVar")

# run the simulation with given number of guide bundle set
iSet = np.int(sys.argv[1])
print("iSet = ",iSet)
TestFitting("fixRand_500run_5percent/GB_%03dset.pdf" % (iSet),iSet,plateGBSet,randVar)
print("Done TestFitting")
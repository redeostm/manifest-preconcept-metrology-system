import numpy as np
from scipy import optimize as opt
from fitting_general import *

def RecalibrateParameter(arrPlate,arrPix,paramsFid):
    # 1. Initialize three types of parameters by using fiducial choice:
    # - criticalParam = [Tc[0],Tc[1],Rc[2],fc,cc[0],cc[1]]
    # - probableParam = [Rc[0],Rc[1],kc[0]]
    # - redundantParam = [kc[1],kc[2],kc[3],kc[4]]
    criticalFid = np.array([paramsFid.Tc[0],paramsFid.Tc[1],paramsFid.Rc[2],paramsFid.fc,paramsFid.cc[0],paramsFid.cc[1]],dtype=np.float64)
    probableFid = np.array([paramsFid.Rc[0],paramsFid.Rc[1],paramsFid.kc[0]],dtype=np.float64)
    redundantFid = np.copy(paramsFid.kc[1:])
        
    criticalLP,criticalHP = criticalFid*0.95,criticalFid*1.05
    probableLP,probableHP = probableFid*0.95,probableFid*1.05
    
    criticalLB = np.minimum(criticalLP,criticalHP) + np.array([-np.abs(paramsFid.Tc[1]*0.05),0.,np.abs(paramsFid.Rc[2]*0.035),0.,0.,0.],dtype=np.float64)
    criticalUB = np.maximum(criticalLP,criticalHP) + np.array([np.abs(paramsFid.Tc[1]*0.05),0.,-np.abs(paramsFid.Rc[2]*0.035),0.,0.,0.],dtype=np.float64)
    probableLB,probableUB = np.minimum(probableLP,probableHP),np.maximum(probableLP,probableHP)
    redundantLB,redundantUB = -5E-6 * np.ones(4), 5E-6 * np.ones(4)
    
    # 2. Calibrate criticalParam --> probableParam --> redundantParam
    # Here, calibrate multiple times
    critical,probable,redundant = np.copy(criticalFid),np.copy(probableFid),np.copy(redundantFid)
    for iteration in range(100):
        criticalOld,probableOld,redundantOld = np.copy(critical),np.copy(probable),np.copy(redundant)
        critical = opt.least_squares(ResidueChangeCritical,criticalOld,
                                     args=(arrPlate,arrPix,probableOld,redundantOld),
                                     bounds=(criticalLB,criticalUB),
                                     ftol=1E-16,xtol=1E-16).x
        probable = opt.least_squares(ResidueChangeProbable,probableOld,
                                     args=(arrPlate,arrPix,critical,redundantOld),
                                     bounds=(probableLB,probableUB),
                                     ftol=1E-16,xtol=1E-16).x
        redundant = opt.least_squares(ResidueChangeRedundant,redundantOld,
                                      args=(arrPlate,arrPix,critical,probable),
                                      bounds=(redundantLB,redundantUB),
                                      ftol=1E-16,xtol=1E-16).x
        
        err = max(np.amax(np.abs((critical-criticalOld)/criticalOld)),
                  np.amax(np.abs((probable-probableOld)/probableOld)),
                  np.amax(np.abs((redundant-redundantOld)/redundantOld)))
        if err < 1E-6:
            break

    newParam = ConstructParameter(critical,probable,redundant)
    return newParam
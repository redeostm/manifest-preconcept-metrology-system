import numpy as np
import matplotlib.pyplot as plt
import cv2
from scipy import interpolate as itpl

class Parameter:
    def __init__(self):
        self.ROC = 3265.
        self.DFOV = 1200.
        self.fc = 7246.376812
        self.Tc = np.array([0,-299.408892,-3261.955597])
        self.Rc = np.array([0,0,0])	# Rotation in units of rad...
        self.kc = np.array([-0.2,0,0,0,0])		# radial distortion coefficients
        self.cc = np.array([1224,1025])
        
        
def GetFiducialParameter(prefix):
    params = Parameter()  # See Parameter::__init__ for the definition of other parameters
    params.Rc = np.array([0,0,0])	# Rotation in units of rad... see below
    
    if prefix == "MANIFEST_sim_001_coord_data_camera-1":
        params.Rc = np.array([0.097242,-0.097242,1.568608])
    elif prefix == "MANIFEST_sim_001_coord_data_camera-2":
        params.Rc = np.array([0.120983,0.032417,-0.522923])
    elif prefix == "MANIFEST_sim_001_coord_data_camera-3":
        params.Rc = np.array([0.043415,0.162026,-2.613643])
        
    return params
    
    
def ChangeFiducialParameter(params,valType,valPercent):
    """
    if valPercent < -1 or valPercent > 1:
        print("Error: value error should be between -1% ~ +1%!!!")
        return params
    """
    frac = 1.+valPercent*0.01
    
    newParams = Parameter()
    newParams.fc = params.fc
    newParams.Tc = np.copy(params.Tc)
    newParams.Rc = np.copy(params.Rc)
    newParams.kc = np.copy(params.kc)
    newParams.cc = np.copy(params.cc)
    
    if valType == "fc":
        newParams.fc *= frac
    elif valType[:2] == "Tc":
        TcTotSq = newParams.Tc[0]**2 + newParams.Tc[1]**2 + newParams.Tc[2]**2
        if valType[2] == '0':
            newParams.Tc[0] = 0.01*valPercent*newParams.Tc[1]
        else:
            newParams.Tc[1] *= frac
        newParams.Tc[2] = -np.sqrt(TcTotSq - newParams.Tc[0]**2 - newParams.Tc[1]**2)
    elif valType[:2] == "Rc":
        newParams.Rc[np.int(valType[2])] *= frac
    elif valType[:2] == "kc":
        if valType[2] == '0':
            newParams.kc[0] *= frac
        else:
            newParams.kc[np.int(valType[2])] = 1E-6*valPercent
    elif valType[:2] == "cc":
        newParams.cc[np.int(valType[2])] *= frac
    else:
        print("Error: type should be either (fc, Tc#, Rc#, kc#, cc#)")
    return newParams
    
    
def ChangeFullFiducialParameter(paramOrig,arrPercent):
    params = ChangeFiducialParameter(paramOrig,"Tc0",arrPercent[0])
    params = ChangeFiducialParameter(params,"Tc1",arrPercent[1])
    params = ChangeFiducialParameter(params,"Rc0",arrPercent[2])
    params = ChangeFiducialParameter(params,"Rc1",arrPercent[3])
    params = ChangeFiducialParameter(params,"Rc2",arrPercent[4])
    params = ChangeFiducialParameter(params,"fc",arrPercent[5])
    params = ChangeFiducialParameter(params,"kc0",arrPercent[6])
    params = ChangeFiducialParameter(params,"kc1",arrPercent[7])
    params = ChangeFiducialParameter(params,"kc2",arrPercent[8])
    params = ChangeFiducialParameter(params,"kc3",arrPercent[9])
    params = ChangeFiducialParameter(params,"kc4",arrPercent[10])
    params = ChangeFiducialParameter(params,"cc0",arrPercent[11])
    params = ChangeFiducialParameter(params,"cc1",arrPercent[12])
    
    return params
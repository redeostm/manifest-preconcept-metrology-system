import numpy as np
import matplotlib.pyplot as plt
import cv2
from scipy import interpolate as itpl

from params import *
from plate_pix_pf_gb import *
from fitting_5percent import *

def TestFitting(filename,Nset,plateGBSet,randVar):

    WIDTH,HEIGHT = 2448,2050

    # Get the fiducial choice for camera 1
    prefix = "MANIFEST_sim_001_coord_data_camera-1"
    params = GetFiducialParameter(prefix)
    plateTarget = ReadPlatePos(prefix)
    plateFiducial = MakePlateFiducial(100,params.DFOV*0.5,params.ROC)
    pixFiducialFid = ConvertPlateToPix(plateFiducial,params)

    pixGBFidSet = np.array([ConvertPlateToPix(plateGB,params) for plateGB in plateGBSet[:Nset]])

    plt.clf()
    fig,axs = plt.subplots(2,5,figsize=(20,8))
    axs = axs.flatten()

    # Parameter variations that changes 13 parameters (2 for Tc[], 3 for Rc[], 1 for fc, 5 for kc[], 2 for cc[])
    Nvar = len(randVar)
    
    arrTcAO,arrTcBO,arrRcAO,arrRcBO,arrRcCO,arrFcO,arrCcAO,arrCcBO,arrKcAO = randVar[:,0],randVar[:,1],randVar[:,2],randVar[:,3],randVar[:,4],randVar[:,5],randVar[:,11],randVar[:,12],randVar[:,6]
    arrTcAE,arrTcBE,arrRcAE,arrRcBE,arrRcCE,arrFcE,arrCcAE,arrCcBE,arrKcAE = [],[],[],[],[],[],[],[],[]

    errBudget = []
    for rv,ivar in zip(randVar,range(1,Nvar+1)):
        newParams = ChangeFullFiducialParameter(params,rv)
        pixTarget = ConvertPlateToPix(plateTarget,newParams)
        pixFiducial = ConvertPlateToPix(plateFiducial,newParams)
        pixGBSet = np.array([ConvertPlateToPix(plateGB,newParams) for plateGB in plateGBSet[:Nset]])
        
        pixFiducialSub = CollectPixWithinFrame(pixFiducial,WIDTH,HEIGHT)
        plateFiducialSub = GetFiducialSubset(plateFiducial,pixFiducialFid,pixFiducialSub)
        
        """
        pixGBSubSet = np.array([CollectPixWithinFrame(pixGB,WIDTH,HEIGHT) for pixGB in pixGBSet])
        plateGBSubSet = np.array([GetFiducialSubset(plateGB,pixGBFid,pixGBSub) for plateGB,pixGBFid,pixGBSub 
                                  in zip(plateGBSet[:Nset],pixGBFidSet,pixGBSubSet)])
        
        print("Shapes of pixGBSet, pixGBSubSet, plateGBSubSet: ",pixGBSet.shape,pixGBSubSet.shape,plateGBSubSet.shape)
        print("pixGBSubSet: ",pixGBSubSet)
        print("plateGBSubSet: ",plateGBSubSet)
        
        pixGBSub = pixGBSubSet.reshape((-1,2))
        plateGBSub = plateGBSubSet.reshape((-1,3))
        """
        pixGBSub,plateGBSub = [],[]
        for tmpPixGB,tmpPlateGB,tmpPixGBFid in zip(pixGBSet,plateGBSet[:Nset],pixGBFidSet):
            tmpPixGBSub = CollectPixWithinFrame(tmpPixGB,WIDTH,HEIGHT)
            tmpPlateGBSub = GetFiducialSubset(tmpPlateGB,tmpPixGBFid,tmpPixGBSub)
            pixGBSub.extend(tmpPixGBSub)
            plateGBSub.extend(tmpPlateGBSub)
            
        pixGBSub,plateGBSub = np.array(pixGBSub),np.array(plateGBSub)
        #print("Shapes of pixGBSub, plateGBSub: ",pixGBSub.shape,plateGBSub.shape)
        
        """
        print("pixFiducialSub: ",pixFiducialSub.shape," pixGBSub: ",pixGBSub.shape,flush=True)
        print("vstack(pixFiducialSub,pixGBSub): ",np.vstack([pixFiducialSub,pixGBSub]).shape)
        return
        """
        plateSub = np.vstack([plateFiducialSub,plateGBSub])
        pixSub = np.vstack([pixFiducialSub,pixGBSub])
        
        estParams = RecalibrateParameter(plateSub,pixSub,params)
        arrTcAE.append(estParams.Tc[0]/params.Tc[1]*100)
        arrTcBE.append((estParams.Tc[1]-params.Tc[1])/params.Tc[1]*100)
        arrRcAE.append((estParams.Rc[0]-params.Rc[0])/params.Rc[0]*100)
        arrRcBE.append((estParams.Rc[1]-params.Rc[1])/params.Rc[1]*100)
        arrRcCE.append((estParams.Rc[2]-params.Rc[2])/params.Rc[2]*100)
        arrFcE.append((estParams.fc-params.fc)/params.fc*100)
        arrCcAE.append((estParams.cc[0]-params.cc[0])/params.cc[0]*100)
        arrCcBE.append((estParams.cc[1]-params.cc[1])/params.cc[1]*100)
        arrKcAE.append((estParams.kc[0]-params.kc[0])/params.kc[0]*100)

        estPixTarget = ConvertPlateToPix(plateTarget,estParams)
        residues = np.sqrt((estPixTarget[:,0]-pixTarget[:,0])**2 + (estPixTarget[:,1]-pixTarget[:,1])**2)*1E3
    
        errBudget.append(np.percentile(residues,99.865))
        print("\r","Done sample %3d" % (ivar),end='',flush=True)

    for ax,orig,est,valType in zip(axs[:-1],
                                   [arrTcAO,arrTcBO,arrRcAO,arrRcBO,arrRcCO,arrFcO,arrCcAO,arrCcBO,arrKcAO],
                                   [arrTcAE,arrTcBE,arrRcAE,arrRcBE,arrRcCE,arrFcE,arrCcAE,arrCcBE,arrKcAE],
                                   ["Tc[1]","Tc[2]","Rc[1]","Rc[2]","Rc[3]","fc","cc[1]","cc[2]","kc[1]"]):
        ax.scatter(orig,est,s=1,rasterized=True)
        ax.set_title(valType)
        ax.set_xlabel("Original error [%]")
        ax.set_ylabel("Estimated error [%]")
    
    axs[-1].hist(errBudget,bins=np.logspace(-1,5,21),label="Median: %G mpix" % (np.median(errBudget)))
    axs[-1].set_xlabel("3-sigma Residue [mpix]")
    axs[-1].set_xscale("log")
    axs[-1].set_ylabel("Frequency")
    axs[-1].legend()
    plt.savefig("output/%s" % (filename),bbox_inches="tight",dpi=150)
import numpy as np
import matplotlib.pyplot as plt
import cv2
from scipy import interpolate as itpl

def ReadPlatePos(prefix):
    rawData = np.genfromtxt("../08_mod01_07_dataWithROCCorrection/%s.dat" % (prefix))
    arrPlt,arrPix = rawData[:,2:5],rawData[:,5:7]
    return arrPlt
    

def ConvertPlateToPix(arrPlate,params):
    
    cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]],dtype=np.float64)
    #print("Before cv2.projectPoints: Rc = ",params.Rc,params.Rc.dtype,flush=True)
    arrPix,jac = cv2.projectPoints(np.copy(arrPlate),np.array(params.Rc,dtype=np.float64),np.array(params.Tc,dtype=np.float64),cameraMatrix,params.kc)
    arrPix = arrPix.reshape(-1,2)
    
    return arrPix

"""
Returns two maps : (Xpix,Ypix) ---> (Xplt) and (Xpix,Ypix) ---> (Yplt)
from given plate alignment/camera distortion parameters and spline interpolation setup.

Input:
- params : Parameter() object that contains plate alignment/camera distortion parameters.
- kind : kind of spline interpolation ('linear','cubic','quintic')
- Ngrid : number of grids from -DFOV/2 to +DFOV/2

Output:
- mapXYpixToXplt : mapping function of (Xpix,Ypix) ---> (Xplt)
- mapXYpixToYplt : mapping function of (Xpix,Ypix) ---> (Yplt)
"""
def MakeMapPixToPlate(params,kind,Ngrid):
    
    """Step 1. Make the grid for X_plate and Y_plate"""
    Xplt,Yplt = np.mgrid[-params.DFOV*0.5:params.DFOV*0.5:Ngrid*1j,-params.DFOV*0.5:params.DFOV*0.5:Ngrid*1j]
    Xplt,Yplt = Xplt.flatten(),Yplt.flatten()
    #print("Dimension of Xplt : ",Xplt.shape)

    """Step 2. Calculate the Z_plate for each X_plate and Y_plate, and stack it"""
    Zplt = np.sqrt(params.ROC**2-Xplt**2-Yplt**2) - params.ROC
    XYZplt = np.vstack([Xplt,Yplt,Zplt]).T
    #print("Dimension of XYZplt : ",XYZplt.shape)

    """Step 3. Project 3D points to an image plane, by using OpenCV"""
    cameraMatrix = np.array([[params.fc,0,params.cc[0]],[0,params.fc,params.cc[1]],[0,0,1]])
    XYpix,Jacob = cv2.projectPoints(XYZplt,params.Rc,params.Tc,cameraMatrix,params.kc)
    XYpix = XYpix.reshape(-1,2)
    #print("Dimension of XYpix : ",XYpix.shape)

    """Step 4. Construct maps to find (Xpix,Ypix) ---> (Xplt) and (Xpix,Ypix) ---> (Yplt)"""
    mapXYpixToXplt = itpl.interp2d(XYpix[:,0],XYpix[:,1],Xplt,kind=kind)
    mapXYpixToYplt = itpl.interp2d(XYpix[:,0],XYpix[:,1],Yplt,kind=kind)

    return mapXYpixToXplt,mapXYpixToYplt

    
    
"""
Returns X/Y/Z plate position from a given X/Y camera position and mappin functions.

Input:
- arrPixPos : X/Y camera position in units of px.
- mapXYpixToXplt : mapping function of (Xpix,Ypix) ---> (Xplt)
- mapXYpixToYplt : mapping function of (Xpix,Ypix) ---> (Yplt)
- params : Parameter() object that contains plate alignment/camera distortion parameters.

Output:
	- XYZplt : X/Y/Z plate position in units of mm.
"""
def ConvertPixToPlate(arrPix,mapXYpixToXplt,mapXYpixToYplt,params):

    """Step 1. Get the Xplt and Yplt"""
    Xplt = np.array([mapXYpixToXplt(x,y) for x,y in arrPix]).flatten()
    Yplt = np.array([mapXYpixToYplt(x,y) for x,y in arrPix]).flatten()
    #print("Dimension of Xplt : ",Xplt.shape)

    """Step 2. Get the Zplt from Xplt and Yplt, and stack it"""
    Zplt = np.sqrt(params.ROC**2-Xplt**2-Yplt**2) - params.ROC
    XYZplt = np.vstack([Xplt,Yplt,Zplt]).T
    #print("Dimension of XYZplt : ",XYZplt.shape)

    return XYZplt


def MakePlateFiducial(N,radius,ROC):
    arrTheta = np.arange(N)*(2.*np.pi/N)
    arrX,arrY = radius*np.cos(arrTheta),radius*np.sin(arrTheta)
    arrZ = np.sqrt(ROC**2-arrX**2-arrY**2) - ROC
    
    return np.vstack([arrX,arrY,arrZ]).T
    

def CollectPixWithinFrame(arrOld,width,height):
    indx = (arrOld[:,0] >= 0) & (arrOld[:,0] < width) & (arrOld[:,1] >= 0) & (arrOld[:,1] < height)
    return arrOld[indx,:]

def GetFiducialSubset(arrPlate,arrPixFid,arrPixNew):
    arrPlateSub = []
    for pixNew in arrPixNew:
        diffSq = (arrPixFid[:,0]-pixNew[0])**2 + (arrPixFid[:,1]-pixNew[1])**2
        indx = np.argmin(diffSq)
        arrPlateSub.append(arrPlate[indx,:])
        
    return np.array(arrPlateSub)
    
    
def MakeGuideBundle(N,radius,ROC):
    arrRand = np.random.rand(N,2)
    arrR,arrTheta = radius*np.sqrt(arrRand[:,0]),2.*np.pi*arrRand[:,1]
    arrX,arrY = arrR*np.cos(arrTheta),arrR*np.sin(arrTheta)
    arrZ = np.sqrt(ROC**2 - arrX**2 - arrY**2) - ROC
    
    return np.vstack([arrX,arrY,arrZ]).T
        
    
def CreateGuideBundleSet(Nset):
    NGB,DFOV,ROC = 19,1200.,3265.
    rawPlateGB = MakeGuideBundle(NGB*Nset,DFOV*0.5,ROC)
    plateGB = rawPlateGB.reshape((Nset,NGB,3))
    return plateGB
    
    

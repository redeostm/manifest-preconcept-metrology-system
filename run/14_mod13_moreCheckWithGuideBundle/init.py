import numpy as np
import matplotlib.pyplot as plt
import cv2
from scipy import interpolate as itpl

from fitting import *
from plate_pix_pf_gb import *

import h5py

# This is for common use of guide bundles and error budget
# Assuming 5%, 500 runs, 100 sets of guide bundles in maximum
plateGBSet = CreateGuideBundleSet(100)
f = h5py.File("input/GB_100set.hdf5","a")
f.create_dataset("PlateGB",data=plateGBSet,dtype='f8')
f.close()

randVar = np.random.rand(500,13)*10.-5.
randVar[:,4] *= 0.3 # Rc[3] should not excess +/- 2%!
f = h5py.File("input/rand_500set_5percent.hdf5","a")
f.create_dataset("RandVar",data=randVar,dtype='f8')
f.close()


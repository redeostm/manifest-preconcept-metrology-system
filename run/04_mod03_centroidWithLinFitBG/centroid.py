#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import skimage.morphology as skmorph
import skimage.filters as skfilter
import skimage.measure as skmsr
import scipy.optimize as optimize
import sys


#strehlv,levelv,pixv,lower1,higher1,win1 = 0,0.45,150,150,180,17
# how to define them?
strehlv,levelv,pixv,win1 = 0,0.45,50,17


'''
Function to perform the centroid finding.

Input:
	- prefix : prefix of the input file name (e.g. MANIFEST_sim_001_coord_data_camera-1)
	- isReference : True if one wants to compare the result with the true value
'''
def GetTotCentroid(prefix,isReference=False):

	'''
	Read processed image data
	'''
	imgNorm,imgBW = ReadImage("input/%s.bmp" % (prefix))

	'''
	If isRefence is True, read the true value to compare
	'''
	referenceTxt = None
	if isReference == True:
		referenceTxt = ReadReference("input/%s.dat" % (prefix))

	'''
	Get the first guess of centroid and write
	'''
	arrRaw,radius = GetRawCentroid(imgBW,isReturnRadius=True)
#WriteToTxt("output/%s.raw" % (prefix),arrRaw,referenceTxt,isReverse=True)

	'''
	Perform the centroid finding with two options : Center-of-Mass and Gaussian fitting
	After that, write the result --- if isReference is True, compare it with the true value.
	'''
	for model in ['COM','Gauss']:
		arrXYPix = GetCentroidModel(arrRaw,radius,imgNorm,model)
		WriteToTxt("output/%s.%s.LinBG" % (prefix,model),arrXYPix,referenceTxt)



'''
Read image data
'''
def ReadImage(filename):
	'''
	Directly read the image.
	It could be either 2- or 3-dimensional, where the later contains RGB
	'''
	oldimg = plt.imread(filename)
	print("min/max/median of oldimg:",oldimg.min(),oldimg.max(),np.median(oldimg))
	print("dimension of oldimg:",oldimg.shape)

	'''
	Fix them into grayscale 2-dimensional image
	'''
	if len(oldimg.shape) == 3:
		img = np.mean(oldimg,axis=2)
	else:
		img = oldimg

	'''
	Process image into black-and-white
	'''
	imgPost = ProcessImage(img,strehlv,levelv)

	return imgPost



'''
Process raw grayscale image into black-and-white

Input:
 - img : Original grayscale image
 - strehl : 
 - level : Threshold value between 0~1 to generate the black-and-white image.

Output:
 - imgNorm : New grayscale image between 0~1. Minimum value is from the median of background.
 - imgBW : Black-and-white image --- 1 if imgNorm is greater than level.
'''
def ProcessImage(img,strehl,level):

	'''
	Step 1. Find background
	'''
	imgBg = np.median(img)*np.ones_like(img)
	if strehl != 0:
		se = skmorph.disk(strehl)
		imgBg = skmorph.opening(img,se)

	'''
	Step 2. Remove background and normalize
	'''
	imgNorm = np.fmax(img-imgBg,0)
	imgNorm /= imgNorm.max()

	'''
	Step 3. Make it as black-and-white with given level, and remove small residues
	'''
	imgBW = (imgNorm > level).astype(np.uint8)

	return imgNorm,imgBW


'''
Read the "true" values of the centroids.
Columns : IDs (#1~2), X/Y/Z plate positions (#3~5, mm), X/Y camera pixels (#6~7, pix)
'''
def ReadReference(filename):

	return np.genfromtxt(filename)



'''
Get the first estimation of object centroid
'''
def GetRawCentroid(img,isReturnRadius=False):

	'''
	Step 1. Get the connected regions
	'''
	cc = skmsr.label(img,4)
	S = skmsr.regionprops(cc)

	print("[before checking area]")
	print("number of connected objects:",len(S))
	areas = np.array([s.area for s in S])
	print("min/max/median of area of connected objects:",areas.min(),areas.max(),np.median(areas))

	'''
	Step 2. Get the centroids of connected regions between areas
	'''
	medArea = np.median(areas)
	arrCentroid = np.array([s.centroid for s in S if medArea/2 <= s.area <= medArea*2])
	print("[after checking area]")
	print("number of connected objects:",len(arrCentroid))

	if isReturnRadius == True:
		return arrCentroid,np.sqrt(medArea)
	else:
		return arrCentroid



'''
Get the centroid from their raw expectations.

Input:
	- arrRaw : Raw expectation of centroids
	- img : Processed image
	- model : Centroid finding model ('COM', 'Gauss')
	- width : Width of the window (pix)

Output: Array of centroids
'''
def GetCentroidModel(arrRaw,radius,img,model,width=win1):

	YSIZE,XSIZE = img.shape
	hw = width*0.5

	arrCentroid = []

	indx = 0
	for raw in arrRaw:
		'''
		Step 1. Define the X/Y range within the window
		'''
		ymin,ymax = max(0,np.int(raw[0]-hw)),min(YSIZE,np.int(raw[0]+hw+1))
		xmin,xmax = max(0,np.int(raw[1]-hw)),min(XSIZE,np.int(raw[1]+hw+1))
		arrY,arrX = np.arange(ymin,ymax),np.arange(xmin,xmax)

		'''
		Step 2. Get the 1D-projected intensities in X/Y-directions
		'''
		subImg = np.copy(img[ymin:ymax,xmin:xmax])
		Xlum,Ylum = subImg.sum(axis=0),subImg.sum(axis=1)

		'''
		Step 3. Get the linear fitting of each direction,
		by masking the radius with Gaussian filter
		'''
		if indx == 0:
			Xlum,Ylum = RemoveLinearBG(Xlum,arrX,raw[1],radius,isPlot=True),RemoveLinearBG(Ylum,arrY,raw[0],radius)
		else:
			Xlum,Ylum = RemoveLinearBG(Xlum,arrX,raw[1],radius),RemoveLinearBG(Ylum,arrY,raw[0],radius)

		'''
		Step 4. Depending on the model, calculate the centroid
		'''
		cenX,cenY = 0,0

		if model=='COM':
			cenX,cenY = np.average(arrX,weights=Xlum),np.average(arrY,weights=Ylum)
		elif model=='Gauss':
			cenX,cenY = GetGaussianCenter(arrX,Xlum),GetGaussianCenter(arrY,Ylum)
		else:
			print("Error: model should be either COM or Gauss!")
			sys.exit(-1)

		if np.isfinite(cenX)==False or cenX <= xmin or cenX >= xmax:
			print("Error: something's wrong to calculate Xcen")
			sys.exit(-1)
		if np.isfinite(cenY)==False or cenY <= ymin or cenY >= ymax:
			print("Error: something's wrong to calculate Ycen")
			sys.exit(-1)

		arrCentroid.append([cenX,cenY])

		indx += 1

	print("Done calculating centroids with ",model)
	return np.array(arrCentroid)



def RemoveLinearBG(Y,X,Xcen,Xrad,isPlot=False):
	s0 = (Y[-1]-Y[0])/(X[-1]-X[0])
	y0 = Y[0] - s0*X[0]

	popt,pcov = optimize.curve_fit(LinFunc,X,Y,p0=[s0,y0],sigma=Y.min()+(Y.max()-Y.min())*np.exp(-0.5*((X-Xcen)/(Xrad))**2))

	if isPlot == True:
		plt.clf()
		plt.figure(figsize=(4,4))

		plt.plot(X,Y,'k-',
				X,LinFunc(X,*popt),'r--')

		plt.savefig("linearBG.pdf",bbox_inches="tight")

	Ynew = Y - LinFunc(X,*popt)
	Ynew[Ynew < 0] = 0

	return Ynew


def LinFunc(x,s,y):
	return s*x+y


'''
Returns the center that has Gaussian intensity profile.

Input:
	- X : Position
	- Y : Intensity, which is believed to have Gaussian shape.

Output: Center
'''
def GetGaussianCenter(X,Y):

	mu0 = np.average(X,weights=Y)
	sigma0 = np.sqrt(np.average(X*X,weights=Y)-mu0**2)
	A0 = Y.max()
	popt,pcov = optimize.curve_fit(GaussFn,X,Y,p0=[sigma0,mu0,A0],bounds=([0,X[0],0],[X[-1]-X[0],X[-1],2*A0]))

	return popt[1]


'''
Gaussian function
'''
def GaussFn(x,sigma,mu,A):

	return A*np.exp(-0.5*((x-mu)/sigma)**2)



'''
Write the centroid result

Input:
	- filename : Name of the output file
	- arrCentroid : Array of centroids
	- referenceTxt : Array of "true" values, or "None"
	- isReverse : True if one needs to reverse the X/Y order of arrCentroid (Raw)`
'''
def WriteToTxt(filename,arrCentroid,referenceTxt=None,isReverse=False):

	newCent = arrCentroid + 1		# We may need this, since the reference value seems to start from 1

	fp = open(filename,"w")

	if referenceTxt is None:
		fp.write("# COL 1-2: Estimated X/Y centroid [pix]\n")
		if isReverse == True:
			for centroid in newCent:
				fp.write("%f  %f\n" % (centroid[1],centroid[0]))
		else:
			for centroid in newCent:
				fp.write("%f  %f\n" % (centroid[0],centroid[1]))

	else:
		fp.write("# COL 1-2: True indices\n")
		fp.write("# COL 3-5: True X/Y/Z plate [mm]\n")
		fp.write("# COL 6-7: True X/Y position [pix]\n")
		fp.write("# COL 8-9: Estimated X/Y centroid [pix]\n")
		fp.write("# COL 10: Centroid error [pix]\n")
		if isReverse == True:
			for item in referenceTxt:
				arrDist = (newCent[:,1]-item[5])**2 + (newCent[:,0]-item[6])**2
				indx = np.argmin(arrDist)
				fp.write("%d  %d  %f  %f  %f  %f  %f  %f  %f  %f\n" %
						(item[0],item[1],item[2],item[3],item[4],item[5],item[6],\
						 newCent[indx,1],newCent[indx,0],np.sqrt(arrDist[indx])))
		else:
			for item in referenceTxt:
				arrDist = (newCent[:,0]-item[5])**2 + (newCent[:,1]-item[6])**2
				indx = np.argmin(arrDist)
				fp.write("%d  %d  %f  %f  %f  %f  %f  %f  %f  %f\n" %
						(item[0],item[1],item[2],item[3],item[4],item[5],item[6],\
						 newCent[indx,0],newCent[indx,1],np.sqrt(arrDist[indx])))

	fp.close()
	print("Done writing to ",filename)


if __name__=="__main__":

	'''
	Read prefix (e.g. MANIFEST_sim_001_coord_data_camera-1)
	'''
	prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))

	'''
	Perform the centroid finding and write
	'''
	GetTotCentroid(prefix,isReference=True)

#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import sys


# Plotting
plt.clf()
plt.figure(figsize=(4,4))

prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))

arrModel = ['COM','Gauss']

arrRes = []
for model in arrModel:
	rawData = np.genfromtxt("output/%s.%s.LinBG" % (prefix,model))
	arrRes.append(rawData[:,9])

arrRes = np.array(arrRes)
vmin,vmax = np.log10(arrRes.min()),np.log10(arrRes.max())
bins = 10.**np.linspace(vmin,vmax,21)
cenBins = np.sqrt(bins[1:]*bins[:-1])

for res,model in zip(arrRes,arrModel):
	hist,_ = np.histogram(res,bins=bins)
	plt.plot(cenBins,hist,label="%s (%.3E pix)" % (model,np.median(res)))

plt.xscale("log")
plt.xlabel("Error [pix]")
plt.ylabel("Frequency")
plt.legend(loc='upper left',fontsize='small')
plt.savefig("plots/%s.LinBG.hist.pdf" % (prefix),bbox_inches="tight")



# Summary table
fp = open("output/%s.LinBG.stats" % (prefix),"w")
fp.write("# COL 1: Centroid model\n")
fp.write("# COL 2: Error at 50% [pixel]\n")
fp.write("# COL 3: Error at 90% [pixel]\n")
fp.write("# COL 4: Error at 95% [pixel]\n")
fp.write("# COL 5: Error at 99% [pixel]\n")

for res,model in zip(arrRes,arrModel):
	fp.write("%s  %E  %E  %E  %E\n"
			% (model,np.median(res),np.percentile(res,90),np.percentile(res,95),np.percentile(res,99)))

fp.close()

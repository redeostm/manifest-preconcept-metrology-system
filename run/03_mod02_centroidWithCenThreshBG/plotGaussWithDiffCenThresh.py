#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import sys


# Plotting
plt.clf()
plt.figure(figsize=(4,4))

prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))
arrCenThresh = [0,30,50,60,70,80,90,100,150,200]

arrRes = []
for cenThresh in arrCenThresh:
	rawData = np.genfromtxt("output/%s.Gauss.%03d" % (prefix,cenThresh))
	arrRes.append(rawData[:,9])

arrRes = np.array(arrRes)
vmin,vmax = np.log10(arrRes.min()),np.log10(arrRes.max())
bins = 10.**np.linspace(vmin,vmax,21)
cenBins = np.sqrt(bins[1:]*bins[:-1])

for res,cenThresh in zip(arrRes,arrCenThresh):
	hist,_ = np.histogram(res,bins=bins)
	plt.plot(cenBins,hist,label="%d %% (%.3E pix)" % (cenThresh,np.median(res)))

plt.xscale("log")
plt.xlabel("Error [pix]")
plt.ylabel("Frequency")
plt.legend(loc='upper left',fontsize='small')
plt.savefig("plots/%s.Gauss.hist.pdf" % (prefix),bbox_inches="tight")



# Summary table
fp = open("output/%s.Gauss.stats" % (prefix),"w")
fp.write("# COL 1: Threshold with respect to the average of the window [%]\n")
fp.write("# COL 2: Error at 50% [pixel]\n")
fp.write("# COL 3: Error at 90% [pixel]\n")
fp.write("# COL 4: Error at 95% [pixel]\n")
fp.write("# COL 5: Error at 99% [pixel]\n")

for res,cenThresh in zip(arrRes,arrCenThresh):
	fp.write("%d  %E  %E  %E  %E\n"
			% (cenThresh,np.median(res),np.percentile(res,90),np.percentile(res,95),np.percentile(res,99)))

fp.close()

#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import sys

prefix = "MANIFEST_sim_001_coord_data_camera-%d" % (np.int(sys.argv[1]))
centhresh = np.int(sys.argv[2])

dataRaw = np.genfromtxt("output/%s.raw" % (prefix))
dataCOM = np.genfromtxt("output/%s.COM.%03d" % (prefix,centhresh))
dataGauss = np.genfromtxt("output/%s.Gauss.%03d" % (prefix,centhresh))

# Scatter plot
plt.clf()
plt.figure(figsize=(4,4))
plt.scatter(dataCOM[:,5],dataCOM[:,6],color='k',s=1)
plt.scatter(dataRaw[:,7],dataRaw[:,8],color='C0',s=1,label="Raw")
plt.scatter(dataCOM[:,7],dataCOM[:,8],color='C1',s=1,label="COM")
plt.scatter(dataGauss[:,7],dataGauss[:,8],color='C2',s=1,label="Gauss")
plt.xlabel("X [pix]")
plt.ylabel("Y [pix]")
plt.legend()
plt.savefig("plots/%s.%03d.scatter.pdf" % (prefix,centhresh),bbox_inches="tight")


# Difference histogram
plt.clf()
plt.figure(figsize=(4,4))
vmin = np.log10(min(dataRaw[:,9].min(),dataCOM[:,9].min(),dataGauss[:,9].min()))
vmax = np.log10(max(dataRaw[:,9].max(),dataCOM[:,9].max(),dataGauss[:,9].max()))
plt.hist(dataRaw[:,9],bins=10.**np.linspace(vmin,vmax,21),color='C0',histtype='step',label="Raw")
plt.hist(dataCOM[:,9],bins=10.**np.linspace(vmin,vmax,21),color='C1',histtype='step',label="COM")
plt.hist(dataGauss[:,9],bins=10.**np.linspace(vmin,vmax,21),color='C2',histtype='step',label="Gauss")
plt.xscale("log")
plt.xlabel("Error [pix]")
plt.ylabel("Frequency")
plt.legend()
plt.savefig("plots/%s.%03d.hist.pdf" % (prefix,centhresh),bbox_inches="tight")
